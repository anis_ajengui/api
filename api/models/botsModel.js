const botSchema = new Schema({
   name: String,
   actions: String,
   users: [],
   password: String,
   permissionLevel: Number
});

const botModel = mongoose.model('Bots', botSchema);

exports.createBot = (BotData) => {
    const bot = new Bot(BotData);
    return bot.save();
};
exports.patchBot = (id, BotData) => {
    return new Promise((resolve, reject) => {
        Bot.findById(id, function (err, Bot) {
            if (err) reject(err);
            for (let i in BotData) {
                Bot[i] = BotData[i];
            }
            Bot.save(function (err, updatedBot) {
                if (err) return reject(err);
                resolve(updatedBot);
            });
        });
    })
};
exports.removeById = (botId) => {
    return new Promise((resolve, reject) => {
        Bot.remove({_id: botId}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};
