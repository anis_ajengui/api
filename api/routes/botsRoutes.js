app.post('/bots', [
    BotsController.insert
]);
app.patch('/bots/:botid', [
    BotsController.patchById
]);
app.delete('/bots/:botid', [
    BotsController.removeById
]);
