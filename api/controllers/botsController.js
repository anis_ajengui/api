exports.insert = (req, res) => {
    BotModel.createBot(req.body)
        .then((result) => {
            res.status(201).send({id: result._id});
        });
};

exports.patchById = (req, res) => {

    BotModel.patchBot(req.params.botId, req.body).then((result) => {
        res.status(204).send({});
    });
};
exports.removeById = (req, res) => {
    BotModel.removeById(req.params.botId)
        .then((result)=>{
            res.status(204).send({});
        });
};
